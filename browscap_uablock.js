(function ($) {

/**
 * Provide the summary information for the Browscap Block (Is Mobile) settings
 * vertical tab.
 */
Drupal.behaviors.BrowscapBlockSettingsSummary = {
  attach: function (context) {

    $('fieldset#edit-browscap-uablock', context).drupalSetSummary(function (context) {
      var vals = [];
      $('input[type="checkbox"]:checked', context).each(function () {
        vals.push($.trim($(this).next('label').text()));
      });
      if(vals.length == 0) {
        vals.push(Drupal.t('Nothing hidden'));
      }
      return vals.join(', ');
    });

  }
};

})(jQuery);
