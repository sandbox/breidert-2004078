Browscap UABlock allows to hide blocks for specific browsers or 
operating systems.

Dependencies
------------

Browscap UABlock depends on the module 
Browscap (http://drupal.org/project/browscap).

Installation
------------

Browscap UABlock can be installed via the standard Drupal installation process.
http://drupal.org/node/895232

Configuration
-------------

Browscap UABlock provides two default filters. (IE 6, WinXP) that can be used 
to used to hide certain blocks. More filters can be added in the configuration 
section of the module (admin/config/system/browscap_uablock). The filters 
support wildcards such as IE* or Chrom*.

To hide a block got to the blocks configuration page and check the filters, 
the block should be hidden for.
